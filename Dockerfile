FROM alpine

RUN apk add --update ca-certificates && \
    rm -rf /var/cache/apk/* /tmp/*

# Fix /lib64/ld-linux-x86-64.so.2
RUN mkdir /lib64 && ln -s /lib/libc.musl-x86_64.so.1 /lib64/ld-linux-x86-64.so.2

ENTRYPOINT ["/opt/app/route53-kubernetes"]
RUN mkdir -p /opt/app
WORKDIR /opt/app

ADD route53-kubernetes /opt/app/route53-kubernetes
